import re
import copy

REGEX = re.compile(r"[^a-zA-Z0-9]")
NO_MATCH = -1


def normalise(txt):
    return REGEX.sub("", txt).lower()


class ColumnMatcher:
    def __init__(
        self,
        patterns=None,
        alts=None,
        no_match_allowed=False,
        allow_partial=False,
        default=None,
    ):
        # Field will be set in the ColumnMapper class
        self.field = None
        self.key = None
        self.patterns = patterns
        self.alts = alts
        self.no_match_allowed = no_match_allowed
        self.allow_partial = allow_partial
        self._default = default

    def match(self, header):
        field_norm = normalise(self.field)
        for col in header:
            col_norm = normalise(col)
            if col_norm == field_norm:
                self.key = col
                break
            if self.allow_partial and col_norm.count(field_norm) > 0:
                self.key = col
                break
            if self.alts:
                alts = [normalise(a) for a in self.alts]
                if col_norm in alts:
                    self.key = col
                    break
            if self.patterns:
                for pattern in self.patterns:
                    if re.search(pattern, col, re.IGNORECASE):
                        self.key = col
                        break
                if self.key:
                    break
        if not self.key:
            if self.no_match_allowed:
                self.key = NO_MATCH
                return
            raise ImportError(
                "The data set must include a {} column".format(self.field)
            )

    def matched(self):
        return self.key is not None

    def default(self):
        if callable(self._default):
            return self._default()
        else:
            return self._default


class ColumnMapperMetaClass(type):
    def __new__(cls, name, bases, attrs, **kwargs):
        fields = []
        for k, v in attrs.items():
            if isinstance(v, ColumnMatcher):
                setattr(v, "field", k)
                fields.append(v)
        attrs["_fields"] = fields
        return super().__new__(cls, name, bases, attrs)


class ColumnMapper(metaclass=ColumnMapperMetaClass):
    def __init__(self, data):
        assert isinstance(data, list)
        assert len(data) > 0
        assert isinstance(data[0], dict)
        self.fields = copy.deepcopy(self._fields)
        self._data = data

        for field in self.fields:
            field.match(data[0])

    def __len__(self):
        return len(self._data)

    def __iter__(self):
        self._idx = 0
        return self

    def __next__(self):
        if self._idx >= len(self._data):
            raise StopIteration
        self._idx += 1
        return self.__getitem__(self._idx - 1)

    def __getitem__(self, key):
        if isinstance(key, slice):
            return [self.remap(el) for el in self._data[key]]
        return self.remap(self._data[key])

    def remap(self, d):
        rtn = {}
        for f in self.fields:
            if f.key == NO_MATCH:
                rtn[f.field] = f.default()
            else:
                rtn[f.field] = d[f.key]
        return rtn

    def data(self):
        return [self.remap(el) for el in self._data]
